import app
import unittest

class Testing(unittest.TestCase):
  def test_greet(self):
    result = app.greet()
    self.assertEqual(result,0)

if __name__ == '__main__':
    unittest.main()
